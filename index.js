let toggleBtn = document.getElementById("toggler");
let basicPlanElement = document.getElementById("basicPlanPrice");
let professionalPlanElement = document.getElementById("professionalPlanPrice");
let masterPlanElement = document.getElementById("masterPlanPrice");


toggleBtn.addEventListener("click", () => {
    if(toggleBtn.checked === true) {
        basicPlanElement.textContent = 19.99;
        professionalPlanElement.textContent = 24.99;
        masterPlanElement.textContent = 39.99;
    } else {
        basicPlanElement.textContent = 199.99;
        professionalPlanElement.textContent = 249.99;
        masterPlanElement.textContent = 399.99;
    }
})
